﻿namespace BookManagement
{
    partial class BookRegister
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.bookRegisterTitle = new System.Windows.Forms.TextBox();
            this.bookRegisterAuthor = new System.Windows.Forms.TextBox();
            this.bookRegisterPrice = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.bookRegisterBookstore = new System.Windows.Forms.CheckBox();
            this.bookRegisterEbook = new System.Windows.Forms.CheckBox();
            this.bookRegisterRental = new System.Windows.Forms.CheckBox();
            this.bookRegisterCyberCafe = new System.Windows.Forms.CheckBox();
            this.bookRegisterOther = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.bookRegisterMemo = new System.Windows.Forms.TextBox();
            this.bookRegisterPicture = new System.Windows.Forms.PictureBox();
            this.button2 = new System.Windows.Forms.Button();
            this.bookRegisterUnread = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.bookRegisterOpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.bookRegisterPicture)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(53, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "タイトル";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(53, 129);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "作者";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(53, 179);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "金額";
            // 
            // bookRegisterTitle
            // 
            this.bookRegisterTitle.Location = new System.Drawing.Point(139, 38);
            this.bookRegisterTitle.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bookRegisterTitle.Multiline = true;
            this.bookRegisterTitle.Name = "bookRegisterTitle";
            this.bookRegisterTitle.Size = new System.Drawing.Size(288, 66);
            this.bookRegisterTitle.TabIndex = 3;
            // 
            // bookRegisterAuthor
            // 
            this.bookRegisterAuthor.Location = new System.Drawing.Point(139, 129);
            this.bookRegisterAuthor.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bookRegisterAuthor.Name = "bookRegisterAuthor";
            this.bookRegisterAuthor.Size = new System.Drawing.Size(288, 22);
            this.bookRegisterAuthor.TabIndex = 4;
            // 
            // bookRegisterPrice
            // 
            this.bookRegisterPrice.Location = new System.Drawing.Point(139, 172);
            this.bookRegisterPrice.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bookRegisterPrice.Name = "bookRegisterPrice";
            this.bookRegisterPrice.Size = new System.Drawing.Size(121, 22);
            this.bookRegisterPrice.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(56, 225);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 15);
            this.label4.TabIndex = 6;
            this.label4.Text = "購入方法";
            // 
            // bookRegisterBookstore
            // 
            this.bookRegisterBookstore.AutoSize = true;
            this.bookRegisterBookstore.Location = new System.Drawing.Point(139, 220);
            this.bookRegisterBookstore.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bookRegisterBookstore.Name = "bookRegisterBookstore";
            this.bookRegisterBookstore.Size = new System.Drawing.Size(113, 19);
            this.bookRegisterBookstore.TabIndex = 7;
            this.bookRegisterBookstore.Text = "書店にて購入";
            this.bookRegisterBookstore.UseVisualStyleBackColor = true;
            // 
            // bookRegisterEbook
            // 
            this.bookRegisterEbook.AutoSize = true;
            this.bookRegisterEbook.Location = new System.Drawing.Point(275, 220);
            this.bookRegisterEbook.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bookRegisterEbook.Name = "bookRegisterEbook";
            this.bookRegisterEbook.Size = new System.Drawing.Size(89, 19);
            this.bookRegisterEbook.TabIndex = 8;
            this.bookRegisterEbook.Text = "電子書籍";
            this.bookRegisterEbook.UseVisualStyleBackColor = true;
            // 
            // bookRegisterRental
            // 
            this.bookRegisterRental.AutoSize = true;
            this.bookRegisterRental.Location = new System.Drawing.Point(139, 246);
            this.bookRegisterRental.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bookRegisterRental.Name = "bookRegisterRental";
            this.bookRegisterRental.Size = new System.Drawing.Size(73, 19);
            this.bookRegisterRental.TabIndex = 9;
            this.bookRegisterRental.Text = "レンタル";
            this.bookRegisterRental.UseVisualStyleBackColor = true;
            // 
            // bookRegisterCyberCafe
            // 
            this.bookRegisterCyberCafe.AutoSize = true;
            this.bookRegisterCyberCafe.Location = new System.Drawing.Point(275, 246);
            this.bookRegisterCyberCafe.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bookRegisterCyberCafe.Name = "bookRegisterCyberCafe";
            this.bookRegisterCyberCafe.Size = new System.Drawing.Size(91, 19);
            this.bookRegisterCyberCafe.TabIndex = 10;
            this.bookRegisterCyberCafe.Text = "ネットカフェ";
            this.bookRegisterCyberCafe.UseVisualStyleBackColor = true;
            // 
            // bookRegisterOther
            // 
            this.bookRegisterOther.AutoSize = true;
            this.bookRegisterOther.Location = new System.Drawing.Point(139, 272);
            this.bookRegisterOther.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bookRegisterOther.Name = "bookRegisterOther";
            this.bookRegisterOther.Size = new System.Drawing.Size(67, 19);
            this.bookRegisterOther.TabIndex = 11;
            this.bookRegisterOther.Text = "その他";
            this.bookRegisterOther.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(56, 305);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(28, 15);
            this.label5.TabIndex = 12;
            this.label5.Text = "メモ";
            // 
            // bookRegisterMemo
            // 
            this.bookRegisterMemo.Location = new System.Drawing.Point(139, 305);
            this.bookRegisterMemo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bookRegisterMemo.Multiline = true;
            this.bookRegisterMemo.Name = "bookRegisterMemo";
            this.bookRegisterMemo.Size = new System.Drawing.Size(288, 122);
            this.bookRegisterMemo.TabIndex = 13;
            // 
            // bookRegisterPicture
            // 
            this.bookRegisterPicture.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bookRegisterPicture.Location = new System.Drawing.Point(499, 38);
            this.bookRegisterPicture.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bookRegisterPicture.Name = "bookRegisterPicture";
            this.bookRegisterPicture.Size = new System.Drawing.Size(221, 254);
            this.bookRegisterPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.bookRegisterPicture.TabIndex = 14;
            this.bookRegisterPicture.TabStop = false;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(307, 479);
            this.button2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(153, 75);
            this.button2.TabIndex = 16;
            this.button2.Text = "登録";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.BookRegisterDecision);
            // 
            // bookRegisterUnread
            // 
            this.bookRegisterUnread.BackColor = System.Drawing.SystemColors.ControlLight;
            this.bookRegisterUnread.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bookRegisterUnread.Location = new System.Drawing.Point(531, 353);
            this.bookRegisterUnread.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bookRegisterUnread.Name = "bookRegisterUnread";
            this.bookRegisterUnread.Size = new System.Drawing.Size(157, 74);
            this.bookRegisterUnread.TabIndex = 15;
            this.bookRegisterUnread.Text = "未読";
            this.bookRegisterUnread.UseVisualStyleBackColor = true;
            this.bookRegisterUnread.Click += new System.EventHandler(this.UnreadButtonClicked);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(531, 305);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(157, 32);
            this.button1.TabIndex = 17;
            this.button1.Text = "画像をアップロード";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.BookRegisterImage);
            // 
            // bookRegisterOpenFileDialog
            // 
            this.bookRegisterOpenFileDialog.Title = "画像を選択してください";
            // 
            // BookRegister
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 566);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.bookRegisterUnread);
            this.Controls.Add(this.bookRegisterPicture);
            this.Controls.Add(this.bookRegisterMemo);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.bookRegisterOther);
            this.Controls.Add(this.bookRegisterCyberCafe);
            this.Controls.Add(this.bookRegisterRental);
            this.Controls.Add(this.bookRegisterEbook);
            this.Controls.Add(this.bookRegisterBookstore);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.bookRegisterPrice);
            this.Controls.Add(this.bookRegisterAuthor);
            this.Controls.Add(this.bookRegisterTitle);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "BookRegister";
            this.Text = "BookRegister";
            ((System.ComponentModel.ISupportInitialize)(this.bookRegisterPicture)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox bookRegisterTitle;
        private System.Windows.Forms.TextBox bookRegisterAuthor;
        private System.Windows.Forms.TextBox bookRegisterPrice;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox bookRegisterBookstore;
        private System.Windows.Forms.CheckBox bookRegisterEbook;
        private System.Windows.Forms.CheckBox bookRegisterRental;
        private System.Windows.Forms.CheckBox bookRegisterCyberCafe;
        private System.Windows.Forms.CheckBox bookRegisterOther;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox bookRegisterMemo;
        private System.Windows.Forms.PictureBox bookRegisterPicture;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button bookRegisterUnread;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.OpenFileDialog bookRegisterOpenFileDialog;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BookManagement
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        // 本の登録フォームを表示
        private void BookRegisterClicked(object sender, EventArgs e)
        {
            BookRegister bookRegister = new BookRegister();
            bookRegister.Show();
        }

        // 読みたいリストの登録フォームを表示
        private void NotReadBookClicked(object sender, EventArgs e)
        {
            NotReadBook notReadBook = new NotReadBook();
            notReadBook.Show();
        }

        // 検索フォームを表示
        private void BookSearchClicked(object sender, EventArgs e)
        {
            BookSearch bookSearch = new BookSearch();
            bookSearch.Show();
        }

        // csvフォームを表示
        private void BookCsvClicked(object sender, EventArgs e)
        {
            BookCsv bookCsv = new BookCsv();
            bookCsv.Show();
        }
    }
}

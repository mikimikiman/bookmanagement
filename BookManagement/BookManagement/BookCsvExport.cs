﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BookManagement
{
    public partial class BookCsvExport : Form
    {
        public BookCsvExport()
        {
            InitializeComponent();
        }

        private void BookCsvExportButtonClicked(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}

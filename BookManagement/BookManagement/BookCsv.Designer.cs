﻿namespace BookManagement
{
    partial class BookCsv
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(128, 63);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(269, 108);
            this.button1.TabIndex = 0;
            this.button1.Text = "インポート(ファイル読み込み)";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.BookCsvImport);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(128, 238);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(269, 108);
            this.button2.TabIndex = 1;
            this.button2.Text = "エクスポート(外部へファイル保存)";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.BookCsvExportClicked);
            // 
            // BookCsv
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(522, 442);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Name = "BookCsv";
            this.Text = "BookCsv";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}
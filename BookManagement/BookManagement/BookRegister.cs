﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BookManagement
{
    public partial class BookRegister : Form
    {
        public BookRegister()
        {
            InitializeComponent();
        }

        // 未読ボタンのクリック処理
        private void UnreadButtonClicked(object sender, EventArgs e)
        {
            if (bookRegisterUnread.Text == "未読")
            {
                this.bookRegisterUnread.Text = "読んだ！";
                this.bookRegisterUnread.BackColor = Color.Gold;

            }
            else
            {
                this.bookRegisterUnread.Text = "未読";
                this.bookRegisterUnread.BackColor = SystemColors.ControlLight;
            }
        }

        // 画像の読み込みと表示
        private void BookRegisterImage(object sender, EventArgs e)
        {
            var bookRegisterPictureIn = bookRegisterOpenFileDialog.ShowDialog();
            if (bookRegisterPictureIn == DialogResult.OK)
            {
                bookRegisterPicture.Image = Image.FromFile(bookRegisterOpenFileDialog.FileName);
            }
        }

        // 入力項目をテキストファイルに登録
        private void BookRegisterDecision(object sender, EventArgs e)
        {
            // テキストファイルの行数を確認（行数でテキストファイルと画像ファイルを紐づけ）
            string bookRegisterStorage = @"..\..\BookRegisterStorage.txt";
            var bookRegisterLines = File.ReadAllLines(bookRegisterStorage);
            int bookRegisterNumber = 100000 + bookRegisterLines.Length; // 本の登録管理番号は100000～499999まで

            // チェックボックスの真偽値を数字に置き換え(書き込み時に','で区切ると真偽値はエラーになったため)
            int brBookstore = 0;
            int brEbook = 0;
            int brRental = 0;
            int brCyberCafe = 0;
            int brOther = 0;

            if (bookRegisterBookstore.Checked)
            {
                brBookstore = 1;
            }

            if (bookRegisterEbook.Checked)
            {
                brEbook = 1;
            }

            if (bookRegisterRental.Checked)
            {
                brRental = 1;
            }

            if (bookRegisterCyberCafe.Checked)
            {
                brCyberCafe = 1;
            }

            if (bookRegisterOther.Checked)
            {
                brOther = 1;
            }

            // テキストファイルへ書き込みと保存

            using (StreamWriter streamWriterRegister = new StreamWriter(bookRegisterStorage, true))
            {
                streamWriterRegister.WriteLine(bookRegisterTitle.Text + ',' +
                    bookRegisterAuthor.Text + ',' +
                    bookRegisterPrice.Text + ',' +
                    brBookstore + ',' +
                    brEbook + ',' +
                    brRental + ',' +
                    brCyberCafe + ',' +
                    brOther + ',' +
                    bookRegisterMemo.Text + ',' +
                    bookRegisterUnread.Text + ',' +
                    bookRegisterNumber);
            }

            // 画像を保存するフォルダがなかったら作成する
            if (!Directory.Exists(@"..\..\picture"))
            {
                Directory.CreateDirectory(@"..\..\picture");
            }

            // 画像に管理番号をつけて保存
            if (bookRegisterPicture.Image != null)
            {
                bookRegisterPicture.Image.Save(@"..\..\picture\" + bookRegisterNumber + ".jpg");
            }

            // フォームをとじる
            this.Close();
        }
    }
}

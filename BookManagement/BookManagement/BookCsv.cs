﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BookManagement
{
    public partial class BookCsv : Form
    {
        public BookCsv()
        {
            InitializeComponent();
        }

        // 本の登録へファイルをインポート
        private void BookCsvImport(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = @"C:";
            openFileDialog.Filter = "CSV Files |*.csv";
            openFileDialog.Title = "データを取り込みたいcsvファイルを選んでください";

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                string bookCsvImportPath = openFileDialog.FileName; // パス取得
                StreamReader bookCsvStreamFile = new StreamReader(bookCsvImportPath, System.Text.Encoding.GetEncoding("SJIS"));

                while (!bookCsvStreamFile.EndOfStream)
                {
                    string bookCsvLine = bookCsvStreamFile.ReadLine();
                    string[] bookCsvData = bookCsvLine.Split(',');

                    using (StreamWriter streamWriterCsv = new StreamWriter(@"..\..\BookRegisterStorage.txt", true, System.Text.Encoding.UTF8))
                    {
                        streamWriterCsv.WriteLine(
                            bookCsvData[0] + ',' +
                            bookCsvData[1] + ',' +
                            bookCsvData[2] + ',' +
                            bookCsvData[3] + ',' +
                            bookCsvData[4] + ',' +
                            bookCsvData[5] + ',' +
                            bookCsvData[6] + ',' +
                            bookCsvData[7] + ',' +
                            bookCsvData[8] + ',' +
                            bookCsvData[9]);
                    }
                }
                BookImport bookImport = new BookImport();
                bookImport.Show();
                bookCsvStreamFile.Close();
                this.Close();
            }
        }

        // 指定の場所へファイルをエクスポート(保留中)
        private void BookCsvExportClicked(object sender, EventArgs e)
        {
            string[] bookTextLines = File.ReadAllLines(@"..\..\BookRegisterStorage.txt", Encoding.UTF8);

            SaveFileDialog bookCsvExport = new SaveFileDialog();
            bookCsvExport.FileName = "新しいファイル.csv";
            bookCsvExport.InitialDirectory = @"C:\";
            bookCsvExport.Filter = " csvファイル（* .csv）| * .csv ";
            bookCsvExport.Title = "保存先のファイルを指定してください";
            bookCsvExport.RestoreDirectory = true;

            if (bookCsvExport.ShowDialog() == DialogResult.OK)
            {
                string bookCsvExportPath = bookCsvExport.FileName; // パスの取得

                using (StreamWriter streamWriterCsvExport = new StreamWriter(bookCsvExportPath, false, System.Text.Encoding.GetEncoding("SJIS"))
                {
                    streamWriterCsvExport.WriteLine(
                            bookTextLines[0] + ',' +
                            bookTextLines[1] + ',' +
                            bookTextLines[2] + ',' +
                            bookTextLines[3] + ',' +
                            bookTextLines[4] + ',' +
                            bookTextLines[5] + ',' +
                            bookTextLines[6] + ',' +
                            bookTextLines[7] + ',' +
                            bookTextLines[8] + ',' +
                            bookTextLines[9]);
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BookManagement
{
    public partial class BookSearch : Form
    {
        public BookSearch()
        {
            InitializeComponent();

            // リストを表示
            Dictionary<string, string> keyValuesRegister = new Dictionary<string, string>();
            Dictionary<string, string> keyValuesNotRead = new Dictionary<string, string>();

            using (StreamReader bookRegisterTextFile = new StreamReader(@"..\..\BookRegisterStorage.txt"))
            {
                while (!bookRegisterTextFile.EndOfStream)
                {
                    string bookRegisterLine = bookRegisterTextFile.ReadLine();
                    string[] bookRegisterData = bookRegisterLine.Split(',');
                    keyValuesRegister.Add(bookRegisterData[0], bookRegisterData[1]);
                }
            }
            foreach (KeyValuePair<string, string> data in keyValuesRegister)
            {
                this.bookList.Items.Add(data.Key);
            }

            using (StreamReader notReadBookTextFile = new StreamReader(@"..\..\NotReadBookStorage.txt"))
            {
                while (!notReadBookTextFile.EndOfStream)
                {
                    string notReadBookLine = notReadBookTextFile.ReadLine();
                    string[] notReadBookData = notReadBookLine.Split(',');
                    keyValuesNotRead.Add(notReadBookData[0], notReadBookData[4]);
                }
            }
            foreach (KeyValuePair<string, string> data in keyValuesNotRead)
            {
                this.bookList.Items.Add(data.Key);
            }
        }

        // "すべて"ボタンをクリックしたらすべての本のタイトルが表示される
        private void BookSearchAllClicked(object sender, EventArgs e)
        {
            // リストに表示されている履歴をクリア
            this.bookList.Items.Clear();

            Dictionary<string, string> keyValuesRegister = new Dictionary<string, string>();
            Dictionary<string, string> keyValuesNotRead = new Dictionary<string, string>();

            using (StreamReader bookRegisterTextFile = new StreamReader(@"..\..\BookRegisterStorage.txt"))
            {
                while (!bookRegisterTextFile.EndOfStream)
                {
                    string bookRegisterLine = bookRegisterTextFile.ReadLine();
                    string[] bookRegisterData = bookRegisterLine.Split(',');
                    keyValuesRegister.Add(bookRegisterData[0], bookRegisterData[1]);
                }
            }
            foreach (KeyValuePair<string, string> data in keyValuesRegister)
            {
                this.bookList.Items.Add(data.Key);
            }

            using (StreamReader notReadBookTextFile = new StreamReader(@"..\..\NotReadBookStorage.txt"))
            {
                while (!notReadBookTextFile.EndOfStream)
                {
                    string notReadBookLine = notReadBookTextFile.ReadLine();
                    string[] notReadBookData = notReadBookLine.Split(',');
                    keyValuesNotRead.Add(notReadBookData[0], notReadBookData[4]);
                }
            }
            foreach (KeyValuePair<string, string> data in keyValuesNotRead)
            {
                this.bookList.Items.Add(data.Key);
            }
        }

        // 登録済みのみを表示
        private void BookSearchOnlyRegisterClicked(object sender, EventArgs e)
        {
            // リストに表示されている履歴をクリア
            this.bookList.Items.Clear();

            Dictionary<string, string> keyValuesRegister = new Dictionary<string, string>();

            using (StreamReader bookRegisterTextFile = new StreamReader(@"..\..\BookRegisterStorage.txt"))
            {
                while (!bookRegisterTextFile.EndOfStream)
                {
                    string bookRegisterLine = bookRegisterTextFile.ReadLine();
                    string[] bookRegisterData = bookRegisterLine.Split(',');
                    keyValuesRegister.Add(bookRegisterData[0], bookRegisterData[1]);
                }
            }
            foreach (KeyValuePair<string, string> data in keyValuesRegister)
            {
                this.bookList.Items.Add(data.Key);
            }
        }

        // 読みたいリストのみ表示
        private void BookSearchOnlyNotRead(object sender, EventArgs e)
        {
            // リストに表示されている履歴をクリア
            this.bookList.Items.Clear();

            Dictionary<string, string> keyValuesNotRead = new Dictionary<string, string>();

            using (StreamReader notReadBookTextFile = new StreamReader(@"..\..\NotReadBookStorage.txt"))
            {
                while (!notReadBookTextFile.EndOfStream)
                {
                    string notReadBookLine = notReadBookTextFile.ReadLine();
                    string[] notReadBookData = notReadBookLine.Split(',');
                    keyValuesNotRead.Add(notReadBookData[0], notReadBookData[4]);
                }
            }
            foreach (KeyValuePair<string, string> data in keyValuesNotRead)
            {
                this.bookList.Items.Add(data.Key);
            }
        }

        // 検索ボタン
        private void BookSearchButtonClicked(object sender, EventArgs e)
        {
            this.bookList.Items.Clear(); // 前回の表示が残ったまま、検索したものが追加して表示されるので一旦リストの中身をクリア
            string bookSearchInputBox = bookSearchBox.Text; // 検索窓で検索したテキストを変数に代入
            string bookSearchListBox = bookList.Text; // リストボックスに表示されるテキストを変数に代入

            if (bookSearchInputBox.Contains(bookSearchListBox)) // もし検索窓へ入力した文字がリストボックスにある文字と一致したら・・・
            {
                List<string> bookSearchAllData = new List<string>(); // Listを初期化して使えるようにする。

                using (StreamReader bookRegisterTextFile = new StreamReader(@"..\..\BookRegisterStorage.txt")) // ここのパスに保管されてるテキストファイル（本の登録用データ）を開けて読み込んで。読み終わったら閉じてね。
                {
                    while (!bookRegisterTextFile.EndOfStream) // テキストファイルの中身が最後の行になるまで繰り替えてして。
                    {
                        string bookRegisterLine = bookRegisterTextFile.ReadLine(); // テキストファイルの中身を1行ずつ読み込んで変数に代入して。
                        string[] bookRegisterData = bookRegisterLine.Split(','); // あ、そうそう。テキストファイル読み込むときに','で一単語一単語を区切って配列に格納して。
                        bookSearchAllData.Add(bookRegisterData[0]); // 配列に格納したデータの添え字の[0]番目がタイトルやから、タイトルをさっきの初期化したListに格納していって。
                    }
                }

                using (StreamReader notReadBookTextFile = new StreamReader(@"..\..\NotReadBookStorage.txt")) // 同じようにじゃあ今度は読みたいリストのファイルも読み込んでいこうか。
                {
                    while (!notReadBookTextFile.EndOfStream) // これも同様に最後の行になるまで処理を繰り返そう。
                    {
                        string notReadBookLine = notReadBookTextFile.ReadLine(); // 1行ずつ読み込んだデータを変数に格納
                        string[] notReadBookData = notReadBookLine.Split(','); // 同じくこれも','で単語をひとつずつバラしたものを配列に格納
                        bookSearchAllData.Add(notReadBookData[0]); // 添え字の[0]にタイトルが入っているのでそれをListに格納。
                    }
                }

                for (int i = 0; i < bookSearchAllData.Count; i++) // リストにある中身がなくなるまで処理を繰り返せ
                {
                    if (bookSearchAllData[i].Contains(bookSearchBox.Text)) // 検索窓に入力した単語とリストに格納した単語が一致したら・・・
                    {
                        this.bookSearchFound.Visible = false; // 検索窓の下にある文字を非表示にする
                        this.bookList.Items.Add(bookSearchAllData[i]); // リストボックスに本のタイトルを追加する（追加したものがリストボックスに表示される）
                    }
                }

                if (bookList.Items.Count == 0) // リストになにもデータがなかったら・・・
                {
                    this.bookSearchFound.Visible = true; // 検索窓の下にある文字を表示する
                    bookSearchFound.Text = "お探しの本は見つかりませんでした"; // 表示する文字はこれ。
                }
            }
        }
    }
}

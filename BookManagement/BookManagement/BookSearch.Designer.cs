﻿namespace BookManagement
{
    partial class BookSearch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bookSearchBox = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.bookList = new System.Windows.Forms.ListBox();
            this.bookSearchFound = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // bookSearchBox
            // 
            this.bookSearchBox.Location = new System.Drawing.Point(47, 30);
            this.bookSearchBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.bookSearchBox.Name = "bookSearchBox";
            this.bookSearchBox.Size = new System.Drawing.Size(247, 22);
            this.bookSearchBox.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(316, 18);
            this.button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(105, 44);
            this.button1.TabIndex = 1;
            this.button1.Text = "検索";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.BookSearchButtonClicked);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(393, 100);
            this.button2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(164, 70);
            this.button2.TabIndex = 3;
            this.button2.Text = "すべて";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.BookSearchAllClicked);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(393, 228);
            this.button3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(164, 70);
            this.button3.TabIndex = 4;
            this.button3.Text = "登録済み";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.BookSearchOnlyRegisterClicked);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(393, 349);
            this.button4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(164, 70);
            this.button4.TabIndex = 5;
            this.button4.Text = "読みたいリスト";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.BookSearchOnlyNotRead);
            // 
            // bookList
            // 
            this.bookList.FormattingEnabled = true;
            this.bookList.ItemHeight = 15;
            this.bookList.Location = new System.Drawing.Point(47, 100);
            this.bookList.Name = "bookList";
            this.bookList.ScrollAlwaysVisible = true;
            this.bookList.Size = new System.Drawing.Size(247, 319);
            this.bookList.TabIndex = 6;
            // 
            // bookSearchFound
            // 
            this.bookSearchFound.AutoSize = true;
            this.bookSearchFound.Location = new System.Drawing.Point(44, 69);
            this.bookSearchFound.Name = "bookSearchFound";
            this.bookSearchFound.Size = new System.Drawing.Size(0, 15);
            this.bookSearchFound.TabIndex = 7;
            // 
            // BookSearch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(643, 508);
            this.Controls.Add(this.bookSearchFound);
            this.Controls.Add(this.bookList);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.bookSearchBox);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "BookSearch";
            this.Text = "BookSearch";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox bookSearchBox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.ListBox bookList;
        private System.Windows.Forms.Label bookSearchFound;
    }
}
﻿namespace BookManagement
{
    partial class NotReadBook
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button2 = new System.Windows.Forms.Button();
            this.notReadBookPicture = new System.Windows.Forms.PictureBox();
            this.notReadBookMemo = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.notReadBookPrice = new System.Windows.Forms.TextBox();
            this.notReadBookAuthor = new System.Windows.Forms.TextBox();
            this.notReadBookTitle = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.notReadBookOpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.notReadBookPicture)).BeginInit();
            this.SuspendLayout();
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(256, 352);
            this.button2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(115, 60);
            this.button2.TabIndex = 34;
            this.button2.Text = "登録";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.NotReadBookDecisionClicked);
            // 
            // notReadBookPicture
            // 
            this.notReadBookPicture.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.notReadBookPicture.Location = new System.Drawing.Point(370, 38);
            this.notReadBookPicture.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.notReadBookPicture.Name = "notReadBookPicture";
            this.notReadBookPicture.Size = new System.Drawing.Size(166, 204);
            this.notReadBookPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.notReadBookPicture.TabIndex = 32;
            this.notReadBookPicture.TabStop = false;
            // 
            // notReadBookMemo
            // 
            this.notReadBookMemo.Location = new System.Drawing.Point(100, 202);
            this.notReadBookMemo.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.notReadBookMemo.Multiline = true;
            this.notReadBookMemo.Name = "notReadBookMemo";
            this.notReadBookMemo.Size = new System.Drawing.Size(217, 98);
            this.notReadBookMemo.TabIndex = 31;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(40, 202);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(22, 12);
            this.label5.TabIndex = 30;
            this.label5.Text = "メモ";
            // 
            // notReadBookPrice
            // 
            this.notReadBookPrice.Location = new System.Drawing.Point(100, 146);
            this.notReadBookPrice.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.notReadBookPrice.Name = "notReadBookPrice";
            this.notReadBookPrice.Size = new System.Drawing.Size(92, 19);
            this.notReadBookPrice.TabIndex = 23;
            // 
            // notReadBookAuthor
            // 
            this.notReadBookAuthor.Location = new System.Drawing.Point(100, 112);
            this.notReadBookAuthor.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.notReadBookAuthor.Name = "notReadBookAuthor";
            this.notReadBookAuthor.Size = new System.Drawing.Size(217, 19);
            this.notReadBookAuthor.TabIndex = 22;
            // 
            // notReadBookTitle
            // 
            this.notReadBookTitle.Location = new System.Drawing.Point(100, 38);
            this.notReadBookTitle.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.notReadBookTitle.Multiline = true;
            this.notReadBookTitle.Name = "notReadBookTitle";
            this.notReadBookTitle.Size = new System.Drawing.Size(217, 54);
            this.notReadBookTitle.TabIndex = 21;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(36, 152);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(29, 12);
            this.label3.TabIndex = 20;
            this.label3.Text = "金額";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(36, 112);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 19;
            this.label2.Text = "作者";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(36, 38);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 12);
            this.label1.TabIndex = 18;
            this.label1.Text = "タイトル";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(395, 257);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(118, 31);
            this.button1.TabIndex = 35;
            this.button1.Text = "画像をアップロード";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.NotReadBookImageClicked);
            // 
            // notReadBookOpenFileDialog
            // 
            this.notReadBookOpenFileDialog.FileName = "openFileDialog1";
            // 
            // NotReadBook
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(605, 454);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.notReadBookPicture);
            this.Controls.Add(this.notReadBookMemo);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.notReadBookPrice);
            this.Controls.Add(this.notReadBookAuthor);
            this.Controls.Add(this.notReadBookTitle);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "NotReadBook";
            this.Text = "NotReadBook";
            ((System.ComponentModel.ISupportInitialize)(this.notReadBookPicture)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.PictureBox notReadBookPicture;
        private System.Windows.Forms.TextBox notReadBookMemo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox notReadBookPrice;
        private System.Windows.Forms.TextBox notReadBookAuthor;
        private System.Windows.Forms.TextBox notReadBookTitle;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.OpenFileDialog notReadBookOpenFileDialog;
    }
}
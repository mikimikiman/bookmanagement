﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace BookManagement
{
    public partial class NotReadBook : Form
    {
        public NotReadBook()
        {
            InitializeComponent();
        }

        private void NotReadBookImageClicked(object sender, EventArgs e)
        {
            // 画像の読み込みと表示
            var notReadBookPictureIn = notReadBookOpenFileDialog.ShowDialog();
            if (notReadBookPictureIn == DialogResult.OK)
            {
                notReadBookPicture.Image = Image.FromFile(notReadBookOpenFileDialog.FileName);
            }
        }

        private void NotReadBookDecisionClicked(object sender, EventArgs e)
        {
            // テキストファイルの行数を確認（行数でテキストファイルと画像ファイルを紐づけ）
            string notReadBookStorage = @"..\..\NotReadBookStorage.txt";
            var notReadBookLines = File.ReadAllLines(notReadBookStorage);
            int notReadBookNumber = 500000 + notReadBookLines.Length; // 読みたいリストの管理番号は500000～999999まで

            // テキストファイルへ書き込みと保存
            using (StreamWriter streamWriterNotRead = new StreamWriter(notReadBookStorage, true))
            {
                streamWriterNotRead.WriteLine(notReadBookTitle.Text + ',' +
                    notReadBookAuthor.Text + ',' +
                    notReadBookPrice.Text + ',' +
                    notReadBookMemo.Text + ',' +
                    notReadBookNumber);
            }

            // 画像を保存するフォルダがなかったら作成する
            if (!Directory.Exists(@"..\..\picture"))
            {
                Directory.CreateDirectory(@"..\..\picture");
            }

            // 画像に管理番号をつけて保存
            if (notReadBookPicture.Image != null)
            {
                notReadBookPicture.Image.Save(@"..\..\picture\" + notReadBookNumber + ".jpg");
            }

            // フォームをとじる
            this.Close();
        }
    }
}
